package fa;

/**
 * This class represents transitions in an automaton by modeling them as
 * labeled edges of a graph. Two transitions can be compared by lexicographical
 * order by their source, target and label.
 */
public class Transition implements Comparable<Transition> {
    private State source;
    private State target;
    private char transitionSymbol;

    /**
     * Creates a new transition from {code source} to {@code target} that is
     * used when {@code symbol} is read.
     * 
     * @param source This will be the source of the new transition.
     * @param target This will be the target of the new transition.
     * @param symbol This will be the label of the new transition.
     */
    public Transition(State source, State target, char symbol) {
        this.source = source;
        this.target = target;
        this.transitionSymbol = symbol;
    }

    /**
     * Returns the state the transition goes out of.
     * 
     * @return The source of the transition.
     */
    public State getSource() {
        return this.source;
    }

    /**
     * Returns the state the transition goes into.
     * 
     * @return The target of the transition.
     */
    public State getTarget() {
        return this.target;
    }
    
    /**
     * Returns the symbol that has to be read in order to move over 
     * {@code this}.
     * 
     * @return The label of the transition.
     */
    public char getTransitionSymbol() {
        return this.transitionSymbol;
    }

    /**
     * Returns {@code true} if {@code this} and {@code trans} have the same 
     * {@code source}, {@code target} and {@code transitionSymbol} and 
     * {@code false} if not.
     * 
     * @param trans The transition {@code this} is compared to.
     * @return Returns whether two transitions are equivalent or not.
     */
    @Override
    public boolean equals(Object trans) {
        if (this.getSource().equals(((Transition) trans).getSource())
                && this.getTarget().equals(((Transition) trans).getTarget())
                && (this.getTransitionSymbol() == ((Transition) trans)
                        .getTransitionSymbol())) {
            return true;
        }
        return false;
    }

    /**
     * Generates a hash code for {@code this} by adding the state number of
     * both {@code target} and {@code source} and adding the Integer
     * representation of the transitionSymbol.
     * 
     * @return Returns a hash code for {@code this}.
     */
    @Override
    public int hashCode() {
        return source.getStateNumber() + target.getStateNumber()
                + transitionSymbol;
    }

    /**
     * Compares two Transitions by the number of their targets targets using
     * increasing order.
     * 
     * @param trans
     *            The transition the one calling the method is compared to.
     * @return The distance between the number of the target states. {@code < 0}
     *         if {@code this < trans}, {@code = 0} if equal, and {@code > 0} if
     *         {@code this > trans}.
     */
    @Override
    public int compareTo(Transition trans) {
        int stateDifference = this.getTarget().getStateNumber()
                - trans.getTarget().getStateNumber();
        char thisTransitionSymbol = this.getTransitionSymbol();
        char otherTransitionSymbol = trans.getTransitionSymbol();
        if (stateDifference != 0) {
            return stateDifference;
        }
        if ((thisTransitionSymbol == LambdaNFA.LAMBDA)
                ^ (otherTransitionSymbol == LambdaNFA.LAMBDA)) {
            if (thisTransitionSymbol == LambdaNFA.LAMBDA) {
                return -1;
            }
            return 1;
        }
        return thisTransitionSymbol - otherTransitionSymbol;
    }

    /**
     * Returns the attributes of the transition in a String within the following
     * order: {@code Source}, {@code Target}, {@code TransitionSymbol} .
     * 
     * @return The Source, the Target and the Symbol the transition holds.
     */
    @Override
    public String toString() {
        StringBuilder transitionString = new StringBuilder("(");
        transitionString.append(
                Integer.toString(this.getSource().getStateNumber()) + ", ");
        transitionString.append(
                Integer.toString(this.getTarget().getStateNumber()) + ") ");
        transitionString.append(this.getTransitionSymbol());
        return transitionString.toString();
    }
}