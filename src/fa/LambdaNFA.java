package fa;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

/**
 * This class represents a non-deterministic finite automaton with spontaneous
 * transitions which can compute if a given String is within its language, can
 * return the longest prefix of a String that is within the language of the
 * automaton and give a String representation of itself.
 */
public class LambdaNFA implements Automaton {
    
    /**
     * The state the automaton is stepping into at first.
     */
    private State start;
    
    /**
     * The number of States the automaton has.
     */
    private final int numberOfStates;
    
    /**
     * Contains the states of the automaton.
     */
    private State[] states;
    
    /**
     * The symbol that is used to represent spontaneous transitions.
     */
    public final static char LAMBDA = '~';

    /**
     * Creates a new Automaton with {@code numberOfStates} states and sets every
     * existing state given in {@code acceptingStates} as an accepting State.
     * 
     * @param numberOfStates
     *            The number of States the automaton will have.
     * @param acceptingStates
     *            All States the Automaton shall accept a word when it stops in
     *            one of those states.
     */
    public LambdaNFA(int numberOfStates, int[] acceptingStates) {
        this.start = new State(0);
        this.numberOfStates = numberOfStates;
        states = new State[numberOfStates];
        states[0] = start;
        for (int i = 1; i < numberOfStates; i++) {
            states[i] = new State(i);
        }
        for (int stateNumber : acceptingStates) {
            if ((stateNumber <= numberOfStates) && (stateNumber > 0)) {
                states[stateNumber - 1].setAccepting(true);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isValidTransition(int source, int target, char symbol) {
        return (source >= 1) && (source <= numberOfStates) && (target >= 1)
                && (target <= numberOfStates)
                && (symbol >= Automaton.FIRST_SYMBOL)
                && ((symbol <= Automaton.LAST_SYMBOL) || (symbol == LAMBDA));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addTransition(int source, int target, char symbol) {
        if (isValidTransition(source, target, symbol)) {
            states[source - 1].addTransition(states[target - 1], symbol);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isElement(String word) {
        String longestPrefix = longestPrefix(word);
        if (longestPrefix != null && longestPrefix.equals(word)) {
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String longestPrefix(String word) {
        State nextStepMarker = new State(numberOfStates + 1);
        Queue<State> stateQueue = new LinkedList<State>();
        for (State state : states) {
            state.precomputeNextSet();
        }
        stateQueue.offer(nextStepMarker);
        stateQueue.offer(states[0]);
        for (State reachableState : start.getNextSet()) {
            stateQueue.offer(reachableState);
        }
        int cursor = -1;
        char symbol = '.';
        StringBuilder checkedPart = new StringBuilder();
        StringBuilder prefix = null;
        while (!stateQueue.isEmpty()) {
            State activeState = stateQueue.poll();
            if (activeState == nextStepMarker) {
                ++cursor;
                if (cursor < word.length()) {
                    stateQueue.offer(nextStepMarker);
                    symbol = word.charAt(cursor);
                }
                checkedPart.append(symbol);
            } else {
                if (activeState.isAcceptingState()) {
                    if (prefix == null) {
                        prefix = new StringBuilder();
                    }
                    prefix.append(checkedPart);
                    checkedPart = new StringBuilder();
                }
                if (cursor < word.length()) {
                    Collection<State> reachableStates = activeState
                            .getTargets(symbol);
                    if (reachableStates != null) {
                        for (State state : reachableStates) {
                            stateQueue.offer(state);
                            stateQueue.addAll(state.getNextSet());
                        }
                    }
                }
            }
        }
        if (prefix == null) {
            return null;
        }
        return prefix.toString().substring(0, prefix.length() - 1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        StringBuilder automatonString = new StringBuilder();
        for (State state : states) {
            automatonString.append(state);
        }
        return automatonString.toString();
    }
}
