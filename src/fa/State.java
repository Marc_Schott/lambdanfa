package fa;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

/**
 * This class is representing states of an automaton that are identified by 
 * their number and save transitions within an adjacency matrix.
 */
public class State {
    
    /**
     * The number of letters within the area specified in the interface 
     * Automaton by {@code Automaton.FIRST_SYMBOL} and 
     * {@code Automaton.LAST_SYMBOL}.
     */
    private static final int ALPHABET_LENGTH = Automaton.LAST_SYMBOL
            - Automaton.FIRST_SYMBOL + 1;
    private final int stateNumber;
    private boolean accepting = false;
    private List<Collection<Transition>> charAdj 
                = new ArrayList<Collection<Transition>>(
            State.ALPHABET_LENGTH + 1);
    private boolean[] transitionsSorted = new boolean[State.ALPHABET_LENGTH
            + 1];
    private Collection<State> nextSet;

    /**
     * Creates a new State that is not accepting.
     * 
     * @param stateNumber The number the state has within the automaton.
     */
    public State(int stateNumber) {
        this.stateNumber = stateNumber + 1;
        for (int i = 0; i < State.ALPHABET_LENGTH + 1; i++) {
            transitionsSorted[i] = false;
            charAdj.add(null);
        }
    }

    private static int getListIndex(char symbol) {
        if (symbol == LambdaNFA.LAMBDA) {
            return 0;
        } else {
            return (symbol - Automaton.FIRST_SYMBOL + 1);
        }
    }

    /**
     * Returns the number the state has within the automaton.
     * 
     * @return The number the state has within the automaton.
     */
    public int getStateNumber() {
        return this.stateNumber;
    }

    /**
     * Returns whether the state is accepting or not.
     * 
     * @return {@code True} if the state is accepting or {@code false} if not.
     */
    public boolean isAcceptingState() {
        return this.accepting;
    }

    /**
     * Sets whether the automaton may accept if it terminates on this state or
     * not.
     * 
     * @param accepting Tells whether the automaton shall accept at this state
     * or not.
     */
    public void setAccepting(boolean accepting) {
        this.accepting = accepting;
    }

    /**
     * Returns a Collection containing all the states that can be reached just
     * by spontaneous transitions from the state calling the method.
     * 
     * @return A Collection that contains all states reachable by
     * lambda-transitions from the State calling it.
     */
    public Collection<State> getNextSet() {
        return this.nextSet;
    }

    /**
     * Adds a new transition to the automaton that goes from {@code this}
     * to the State {@code target} when the automaton {@code symbol} is read.
     * 
     * @param target The state that shall be the target of the new transition.
     * @param symbol The symbol the automaton must read to change the state.
     */
    public void addTransition(State target, char symbol) {
        int position = getListIndex(symbol);
        Transition newTransition = new Transition(this, target, symbol);
        if (charAdj.get(position) == null) {
            charAdj.set(position, new LinkedList<Transition>());
        }
        if (!(charAdj.get(position).contains(newTransition))) {
            charAdj.get(position).add(newTransition);
            transitionsSorted[position] = false;
        }
    }

    /**
     * Returns a collection containing all transitions that the automaton can 
     * choose from when {@code symbol} is read by the automaton where
     * spontaneous transitions going out from {@code this] aren't included.
     * 
     * @param symbol The symbol the outgoing transitions to are searched for.
     * @return Returns a collection of states that can be reached by reading 
     * {@code symbol}
     */
    public Collection<Transition> getTransitions(char symbol) {
        if ((symbol < Automaton.FIRST_SYMBOL || symbol > Automaton.LAST_SYMBOL)
                && (symbol != LambdaNFA.LAMBDA)) {
            return null;
        }
        return this.charAdj.get(getListIndex(symbol));
    }
    
    /**
     * Returns the collection containing the states that can be reached by
     * reading {@code symbol} while neglecting any spontaneous transition that
     * can be made before reading {@code symbol}
     * 
     * @param symbol The symbol that is read by the automaton
     * @return The list of all States that can be reached by reading {@code
     * symbol} from {@code this} by using the transitions annotated with 
     * {@code symbol} first.
     */
    public Collection<State> getTargets(char symbol) {
        Collection<State> targets = new LinkedList<State>();
        Collection<Transition> transitions = this.getTransitions(symbol);
        if (transitions != null) {
            for (Transition transition : transitions) {
                State target = transition.getTarget();
                targets.add(target);
                if (target.getNextSet() != null) {
                    targets.addAll(target.getNextSet());
                }
            }
        }
        return targets;
    }

    /**
     * Computes all states that can be reached by just using lambda-transitions
     * from {@code this} and saves them within the attribute {@code nextSet} of
     * {@code this}
     */
    public void precomputeNextSet() {
        nextSet = new LinkedList<State>();
        Set<State> visited = new HashSet<State>();
        Queue<State> bfsQueue = new LinkedList<State>();
        bfsQueue.offer(this);
        visited.add(this);
        while (!bfsQueue.isEmpty()) {
            State state = bfsQueue.poll();
            if (state != this) {
                nextSet.add(state);
            }
            Collection<State> lambdaTargets = state
                    .getTargets(LambdaNFA.LAMBDA);
            if (lambdaTargets != null) {
                for (State lambdaTarget : lambdaTargets) {
                    if (!visited.contains(lambdaTarget)) {
                        bfsQueue.offer(lambdaTarget);
                        visited.add(lambdaTarget);
                    }
                }
            }
        }
    }

    /**
     * Compares two States by comparing their Number within the automaton.
     * 
     * @param state The state {@code this} is compared to.
     * @return Returns whether {@code state} and {@code this} are equivalent or
     * not. 
     */
    @Override
    public boolean equals(Object state) {
        if (this.getStateNumber() == ((State) state).getStateNumber()) {
            return true;
        }
        return false;
    }

    /**
     * Generates a hash code for {@this} by returning its {@code stateNumber}.
     * 
     * @return Returns the hash code.
     */
    @Override
    public int hashCode() {
        return stateNumber;
    }

    /**
     * Sorts the outgoing transitions of a state if necessary by their target
     * states within increasing order.
     */
    public void sortTransitions() {
        if (!transitionsSorted[getListIndex(LambdaNFA.LAMBDA)]) {
            Collection<Transition> lambdaTransitions = this
                    .getTransitions(LambdaNFA.LAMBDA);
            if (lambdaTransitions != null) {
                Collections.sort((List<Transition>) lambdaTransitions);
                this.transitionsSorted[getListIndex(LambdaNFA.LAMBDA)] = true;
            }
        }
        for (char c = Automaton.FIRST_SYMBOL; c < Automaton.LAST_SYMBOL
                + 1; c++) {
            if (!transitionsSorted[getListIndex(c)]) {
                Collection<Transition> transitions = this.getTransitions(c);
                if (transitions == null) {
                    continue;
                }
                Collections.sort((List<Transition>) transitions);
                this.transitionsSorted[getListIndex(c)] = true;
            }
        }
    }

    /**
     * Creates a String representation of all transitions going out of
     * {@code this}.
     * 
     * @return Returns String representations of all outgoing transitions. 
     */
    @Override
    public String toString() {
        List<Transition> outgoingTransitions = new LinkedList<Transition>();
        StringBuilder stateString = new StringBuilder();
        Collection<Transition> outgoingLambdaTransitions = this
                .getTransitions(LambdaNFA.LAMBDA);
        if (outgoingLambdaTransitions != null) {
            outgoingTransitions.addAll(this.getTransitions(LambdaNFA.LAMBDA));
        }
        for (char c = Automaton.FIRST_SYMBOL; c <= Automaton.LAST_SYMBOL; c++) {
            Collection<Transition> transitions = this.getTransitions(c);
            if (transitions != null) {
                outgoingTransitions.addAll(transitions);
            }
        }
        Collections.sort(outgoingTransitions);
        for (Transition transition : outgoingTransitions) {
            stateString.append(transition);
            stateString.append("\n");
        }
        return stateString.toString();
    }
}